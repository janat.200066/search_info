import pytest
from httpx import AsyncClient
from asgi_lifespan import LifespanManager
from fastapi import FastAPI
from typing import Generator

from services.api_gateway.app import gateway as gateway_api


@pytest.fixture(scope='session')
async def test_app():
    async with LifespanManager(gateway_api):
        yield gateway_api


@pytest.fixture(scope='session')
async def gateway_client(test_app: FastAPI) -> Generator:
    async with AsyncClient(
            app=test_app,
            base_url='http://localhost:8000'
    ) as client:
        yield client
