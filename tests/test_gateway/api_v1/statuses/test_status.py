import pytest
from httpx import AsyncClient
import logging

from services.api_gateway.app import gateway as gateway_api


logging.basicConfig(level=logging.DEBUG)


@pytest.mark.asyncio
async def test_status_gateway():
    async with AsyncClient(
            app=gateway_api,
            base_url='http://localhost:8000'
    ) as client:
        response = await client.get('/')
        assert response.status_code == 200
        assert response.json() == "Welcome search info service"

