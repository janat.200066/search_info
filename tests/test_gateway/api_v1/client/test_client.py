import pytest
from httpx import AsyncClient
import logging

from config.tags import uri_api
from services.api_gateway.app import gateway as gateway_api


logging.basicConfig(level=logging.DEBUG)


@pytest.mark.asyncio
async def test_status_client():
    async with AsyncClient(
            app=gateway_api,
            base_url='http://localhost:8000'
    ) as client:
        response = await client.get(uri_api["client"]['start'])
        assert response.status_code == 200
        assert response.json() == "welcome client router"


@pytest.mark.asyncio
async def test_status_client():
    async with AsyncClient(
            app=gateway_api,
            base_url='http://localhost:8000'
    ) as client:
        response = await client.get(uri_api["client"]['data'])
        assert response.status_code == 200
        assert response.json() == [1, 2, 3]
