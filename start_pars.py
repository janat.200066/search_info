import asyncio
import time

from services.parsing.apps.lalafo.main import start_main


if __name__ == '__main__':
    start = time.time()
    asyncio.run(start_main())
    end = time.time()
    print("runner time on seconds: {:.4f}".format(end - start))
