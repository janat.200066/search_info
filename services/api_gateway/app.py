from fastapi import FastAPI, responses

from services.api_gateway.routes.routes import api_v1

gateway = FastAPI(
    debug=True,
    title='Gateway api'
)


@gateway.get("/")
async def welcome():
    return responses.JSONResponse(content="Welcome search info service")


gateway.include_router(api_v1, prefix='/api/v1')
