from fastapi import APIRouter

from config.tags import gateway_api_v1
from services.client.api import client_api

api_v1 = APIRouter()

api_v1.include_router(client_api, prefix='/client', tags=gateway_api_v1['client'])

