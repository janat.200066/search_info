from services.db.deps import get_consumer_session
from services.db.base import Item


async def insert_item(
    item_id: int,
    ad_label: str,
    title: str,
    description: str,
    category_id: int,
    mobile: str,
    **kwargs
):
    async with get_consumer_session() as session:
        item = Item(
                item_id=item_id,
                ad_label=ad_label,
                title=title,
                description=description,
                category_id=category_id,
                mobile=mobile
            )
        session.add(item)
        await session.commit()
