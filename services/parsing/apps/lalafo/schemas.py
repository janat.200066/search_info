from dataclasses import dataclass

from pydantic import BaseModel


@dataclass()
class CategoryTypeId:
    LONG_RENT: int = 2044
    ONE_DAY_RENT: int = 2045
    FOR_SALE: int = 2046


COMPANY_SHOW = ("campaign_show", bool)
CURRENCY = ("currency", "USD KGS")


def to_camel(string: str) -> str:
    string_split = string.split("_")
    return string_split[0] + "".join(word.capitalize() for word in string_split[1:])


class Meta(BaseModel):
    current_page: int
    page_count: int
    per_page: int
    total_count: int

    class Config:
        alias_generator = to_camel


class LinkHref(BaseModel):
    href: str


class Link(BaseModel):
    first: LinkHref
    last: LinkHref
    next: LinkHref
    self: LinkHref


class Item(BaseModel):
    id: int
    images: list
    ad_label: str
    campaign_show: bool
    category_id: int
    city: str
    city_alias: str
    city_id: int
    country_id: int
    created_time: int
    updated_time: int
    currency: str | None
    description: str
    lat: float
    lng: float
    mobile: str | None
    national_price: dict | None
    price: int | None
    title: str
    url: str
    user: dict  # под вопросам
