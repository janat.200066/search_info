import asyncio
from pprint import pprint
import time

import aiohttp

from services.broker.producer import _producer
from services.parsing.apps.lalafo.schemas import Link, Meta, Item


cookies = {
    'event_user_hash': '55485694-1381-41f2-a888-a43b82dca443',
    '_gcl_au': '1.1.1747684050.1686122989',
    '_fbp': 'fb.1.1686122990656.1175563790',
    '_ga': 'GA1.1.1771234470.1686122990',
    '__cf_bm': '12UL3cbAaPndj3p4U4.CgcUcy.l4HxpfIzOm7w1u9qc-1687024361-0-AZ2xjehTsah/9RdunsmJSvYIudFfV/Jnjp0pV3s0VBn3nokfaP7rl21aQpRAQRCuDQ==',
    'experiment': 'novalue',
    'event_session_id': 'dd6a8d82047f51638a96b21b09d91b45',
    'paid-background': '{%2212-1-0%22:{%22458%22:1%2C%22505%22:1%2C%22602%22:1%2C%22603%22:1}%2C%2212-2-2040%22:{%22603%22:4}}',
    'lastAnalyticsEvent': 'listing:feed:listing:ad:tap',
    'device_fingerprint': 'ef4ff84dc1bf4b399b5435036e6bf106',
    '_ga_Z4VRYJ4XGN': 'GS1.1.1687024359.8.1.1687025240.0.0.0',
}

headers = {
    'authority': 'lalafo.kg',
    'accept': 'application/json, text/plain, */*',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'authorization': 'Bearer',
    # 'cookie': 'event_user_hash=55485694-1381-41f2-a888-a43b82dca443; _gcl_au=1.1.1747684050.1686122989; _fbp=fb.1.1686122990656.1175563790; _ga=GA1.1.1771234470.1686122990; __cf_bm=12UL3cbAaPndj3p4U4.CgcUcy.l4HxpfIzOm7w1u9qc-1687024361-0-AZ2xjehTsah/9RdunsmJSvYIudFfV/Jnjp0pV3s0VBn3nokfaP7rl21aQpRAQRCuDQ==; experiment=novalue; event_session_id=dd6a8d82047f51638a96b21b09d91b45; paid-background={%2212-1-0%22:{%22458%22:1%2C%22505%22:1%2C%22602%22:1%2C%22603%22:1}%2C%2212-2-2040%22:{%22603%22:4}}; lastAnalyticsEvent=listing:feed:listing:ad:tap; device_fingerprint=ef4ff84dc1bf4b399b5435036e6bf106; _ga_Z4VRYJ4XGN=GS1.1.1687024359.8.1.1687025240.0.0.0',
    'country-id': '12',
    'device': 'pc',
    'experiment': 'novalue',
    'if-none-match': 'W/"5308a-Byfi/r6YBXzjsovfSjONSw1bxvY"',
    'language': 'ru_RU',
    'referer': 'https://lalafo.kg/bishkek/kvartiry',
    'request-id': 'react-client_50642509-a23c-475c-b516-33eab7bd16da',
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'user-hash': '55485694-1381-41f2-a888-a43b82dca443',
}


async def pars_lalafo(params):

    async with aiohttp.ClientSession() as session:
        async with session.get('https://lalafo.kg/api/search/v3/feed/search',
                               params=params, headers=headers, cookies=cookies) as response:
            response_json = await response.json()
            links = Link(**response_json["_links"])
            meta = Meta(**response_json["_meta"])
            items = response_json["items"]
            for item in items:
                await _producer(message=Item(**item).json())


async def start_main():
    tasks = []

    for i in range(1, 268):
        params = {
            'category_id': '2040',
            'city_id': '103184',
            'expand': 'url',
            'page': i,
        }
        tasks.append(
           asyncio.create_task(pars_lalafo(params))
        )

    for task in tasks:
        await task


if __name__ == '__main__':
    start = time.time()
    asyncio.run(start_main())
    end = time.time()
    print("runner time on seconds: {:.4f}".format(end - start))
