from sqlalchemy import Column, String, Integer, Boolean, Text

from services.db.base_model import Model


class Client(Model):
    pk = Column(Integer, primary_key=True)
    transaction_id = Column(Integer, nullable=False)
    is_active = Column(Boolean, default=False)
    phone_number = Column(String, nullable=False)


class Item(Model):
    pk = Column(Integer, primary_key=True)
    item_id = Column(Integer)
    ad_label = Column(String(255))
    title = Column(String(255))
    description = Column(Text)
    category_id = Column(Integer)
    campaign_show = Column(Boolean)
    city = Column(String(50))
    currency = Column(String(10))
    lat = Column(String(20))
    lng = Column(String(20))
    mobile = Column(String(30))
    price = Column(Integer)
    url = Column(String)

    @classmethod
    def save(cls, **kwargs):
        return cls(**kwargs)
