from fastapi import APIRouter

client_api = APIRouter()


@client_api.get('/')
async def welcome():
    return "welcome client router"


@client_api.get('/data')
async def get_data():
    return [1, 2, 3]
