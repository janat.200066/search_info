import contextlib
from typing import Generator

from sqlalchemy.ext.asyncio import AsyncSession
from services.db.session import Session


async def get_session() -> Generator:
    async with Session() as session:
        yield session


@contextlib.asynccontextmanager
async def get_consumer_session() -> AsyncSession:
    async with Session() as session:
        yield session
