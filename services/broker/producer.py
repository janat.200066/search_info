import aio_pika

from services.broker.base import ROUTING_KEY


async def _producer(message) -> None:
    connection = await aio_pika.connect_robust(
        "amqp://guest:guest@127.0.0.1/",
    )

    async with connection:
        routing_key = ROUTING_KEY

        channel = await connection.channel()
        queue = await channel.declare_queue(
            ROUTING_KEY,
            durable=True
        )

        await channel.default_exchange.publish(
            aio_pika.Message(body=f"{message}".encode()),
            routing_key=routing_key,
        )
