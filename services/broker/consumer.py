import asyncio
import json

import aio_pika

from services.broker.base import ROUTING_KEY
from services.parsing.apps.lalafo.services import insert_item
from services.parsing.apps.lalafo.schemas import Item


async def process_message(
    message: aio_pika.abc.AbstractIncomingMessage,
) -> None:
    async with message.process():
        item = Item(**json.loads(message.body))
        await insert_item(
            item_id=item.id,
            ad_label=item.ad_label,
            title=item.title,
            description=item.description,
            category_id=item.category_id,
            mobile=item.mobile
        )
        await asyncio.sleep(1)


async def main() -> None:
    connection = await aio_pika.connect_robust(
        "amqp://guest:guest@127.0.0.1/",
    )

    queue_name = ROUTING_KEY

    # Creating channel
    channel = await connection.channel()

    # Maximum message count which will be processing at the same time.
    await channel.set_qos(prefetch_count=100)

    # Declaring queue
    queue = await channel.declare_queue(queue_name, durable=True)

    await queue.consume(process_message)

    try:
        # Wait until terminate
        await asyncio.Future()
    finally:
        await connection.close()


if __name__ == "__main__":
    asyncio.run(main())
