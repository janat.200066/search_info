from pathlib import Path
from typing import Any

from pydantic.v1 import BaseSettings, validator, PostgresDsn
from pydantic import AnyHttpUrl

# from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    BASE_DIR: Path = Path(__file__).resolve(strict=True).parent.parent

    SERVER_NAME: str = "COLLECTIONS_CORE"
    SERVER_HOST: str = "http://localhost:8000"
    SECRET_KEY: str = "123123418234814238"

    BACKEND_CORS_ORIGINS: list[str] = ["*"]

    POSTGRES_SERVER: str = "localhost"
    POSTGRES_USER: str = "zhanat"
    POSTGRES_PASSWORD: str = "1"
    POSTGRES_PORT: int = 5432
    POSTGRES_DB: str = "search_info"
    DB_POOL_SIZE: int = 5
    DB_MAX_OVERFLOW: int = 15
    SQLALCHEMY_DATABASE_URI: PostgresDsn | None = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: str | None, values: dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
        )

    MEDIA_ROOT: str = str(BASE_DIR / "media")
    MEDIA_URL: str = "/media/"
    STATIC_ROOT: Path = BASE_DIR / "static"

    class Config:
        env_file = ".env"


settings = Settings()
