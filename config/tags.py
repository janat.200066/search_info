gateway_api_v1 = {
    'client': ['Client']
}

api_v1 = '/api/v1'

uri_api = {
    'client': {
        'start': f'{api_v1}/client/',
        'data': f'{api_v1}/client/data'
    }
}