import uvicorn


if __name__ == '__main__':
    uvicorn.run('services.api_gateway.app:gateway', reload=True, workers=2)
